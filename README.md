# Моя вторая задача от [Deusops](https://deusops.com/)

# Условия и ссылки
<details>
<summary>Light:</summary>

1. Разверните виртуальную машину с linux: game01. Можно использовать любые способы создания, например в Яндекс Облаке или локально на Vagrant.
2. Установите и настройте nginx на виртуальную машину "game01". Настройте сервер в качестве proxy на localhost.
3. Разверните на виртуальной машине Игру и настройте nginx для открытия сайта с игрой.
4. Создайте linux system unit, отвечающий за запуск сервиса с игрой.
5. Сделайте простейшую автоматизацию для настройки сервера: bash-скрипты либо vagrant provisioning.

</details>

<details>
<summary>Normal:</summary>

1. Написать Dockerfile для игры (проект использует node16), нужно сделать образ легковесным через мультистейджинг.
2. Установите на "game01" Docker и запустите игру в контейнере. Настройте работу контейнера без внешнего nginx.
3. Написать gitlab-ci пайплайн для автоматической сборки и публикации Docker-образа с приложением.
4. Написать ansible-роль "docker", для установки docker. Роль должна проходить тесты molecule.
5. Написать ansible-playbook настраивающий сервер "game01", и темплейт system unit запускающим docker-контейнер с нужным нам приложением.
6. Добавить в gitlab-ci шаг с деплоем приложения на сервера. Ansible-роли должны находиться в отдельных репозиториях и работать через ansible-galaxy.
7. Увеличить количество серверов до трех и настроить распределение входящих запросов через haproxy.

</details>

<details>
<summary>Hard:</summary>

1. Развернуть в облаке kubernetes-кластер и настроить ingress для доступа извне.
2. Создать helm чарт для деплоя приложения в kubernetes-кластер.
3. Масштабировать приложение на несколько реплик и настроит балансировку.
4. Доработать пайплайн для автоматического развертывания приложения в kubernetes-кластер.

</details>

<details>
<summary>Expert:</summary>

1. Создать документацию с описанием архитектуры, настроек и процедур обслуживания для вашей инфраструктуры.
2. Оформить развертывание все инфраструктуры в Яндекс Облаке через Terraform.
3. Провести аудит получившейся системы и расписать возможные шаги улучшения.

</details>

<details>
<summary>Links:</summary>

- [Как поднимать виртуальные машины в Vagrant](https://gitlab.com/deusops/lessons/documentation/vagrant)
- [Создание system unit](https://habr.com/ru/companies/slurm/articles/255845/)
- [Подборка информации по Docker](https://gitlab.com/deusops/lessons/documentation/docker)
- [Подборка информации по Gitlab CI](https://gitlab.com/deusops/lessons/documentation/gitlab-ci)
- [Подборка информации по Ansible и написанию ролей](https://gitlab.com/deusops/lessons/documentation/ansible)
- [Подборка информации по Terraform](https://gitlab.com/deusops/documentation/terraform)
- [Хорошее туториал-видео по Terraform](https://youtu.be/SLB_c_ayRMo)
- [Игра (исходники приложения)](https://gitfront.io/r/deusops/JnacRhR4iD8q/2048-game/)

</details>

## Используемые репозитории
- [Репозиторий с приложением](https://gitlab.com/va1erify-deusops/task-26-01-2024/2048-game)
- [Terraform модули для YC](https://gitlab.com/va1erify-terraform/yandex-modules/modules)
- [CI template для Ansible](https://gitlab.com/va1erify-gitlab-ci/ansible-ci)
- [CI template для использования Gitlab в качестве backend для Terraform](https://gitlab.com/va1erify-gitlab-ci/gitlab-terraform-ci)
- [Ansible роль для установки и запуска docker&docker-compose](https://gitlab.com/va1erify-ansible-roles/ansible-role-docker)
- [Ansible роль для установки и настройки hapoxy](https://gitlab.com/va1erify-ansible-roles/ansible-role-haproxy)
- [Ansible роль для создания systemd.service из шаблона, его включения и запуска](https://gitlab.com/va1erify-ansible-roles/ansible-role-systemd)
- [Универсальный манифест для ingress-nginx controller](https://gitlab.com/va1erify-k8s-entities/ingress)
- [Универсальный манифест для cert-manager](https://gitlab.com/va1erify-k8s-entities/cert-manager)


## Light
- [Monorepo](https://gitlab.com/va1erify-deusops/task-26-01-2024/light)

## Normal
- [Monorepo](https://gitlab.com/va1erify-deusops/task-26-01-2024/normal)

## Hard
- [Infrastructure](https://gitlab.com/va1erify-deusops/task-26-01-2024/hard-infrastructure)
- [CD](https://gitlab.com/va1erify-deusops/task-26-01-2024/hard-cd)
- [Variables](https://gitlab.com/va1erify-deusops/task-26-01-2024/hard-gitlab-variables)


## Аудит и возможные шаги улучшения
### Light уровень
1. **Инструменты и автомазизация**
   - Создание инфрастуктуры осуществляется через вынесенные тегированние Terraform модули
   - Провижн и доставка новых версий приложений осуществляется через bash скрипты
   
2. **Безопасность**
   - Для хранения закрытого ключа для VM используется Gitlab CI/CD variables (переменная является masked)
   - Для хранения токена доступа к Yandex Cloud используется Gitlab CI/CD variables (переменная является masked)

3. **Масштабируемость и отказоустойчивость**
    - Инфрастуктура отказоустойчива за счет ее предоставления as Service
    - Легкая масштабируемость за счет создания и управления инфрастуктурой через Terraform

4. **Мониторинг и логирование**
   - Отсутствуют

5. **Согласованность с best practies**
   - Большие затраты на поддержание bash скриптов (нерационально)
   - Нет разделения на контура
   - Монорепозиторий со скриптами и Terraform
   - Нет сертификатов
   - Нет выноса вспомогательного gitlab-ci.yml для хранения состояния в Gitlab backend в отдельный репозиторий для переиспользования
   - Скрипты заточены только под Debian
   - systemd.service запускает скрипт для старта приложения
   - Приложение предоставляется в виде архива (подумать над контейнеризацией)
   - Создание и провижн инфраструктуры осуществляется через один pipeline

### Normal уровень
1. **Инструменты и автомазизация**
   - Создание инфрастуктуры осуществляется через вынесенные тегированние Terraform модули
   - Провижн реализован через ansible (с использованием ansible-galaxy)
   - Приложение поставляется в виде image
   - 3 VM на которых развернуто приложение
   - Балансировка через четвертую VM с haproxy
   
2. **Безопасность**
   - Для хранения закрытого ключа для VM используется Gitlab CI/CD variables (переменная является masked)
   - Для хранения токена доступа к Yandex Cloud используется Gitlab CI/CD variables (переменная является masked)

3. **Масштабируемость и отказоустойчивость**
    - Инфрастуктура отказоустойчива за счет ее предоставления as Service
    - Легкая масштабируемость за счет использования связки Terraform + Ansible, а также поставки приложения в виде образа

4. **Мониторинг и логирование**
   - Отсутствуют

5. **Согласованность с best practies**
   - Нет разделения на контура
   - Монорепозиторий с Ansible и Terraform
   - Нет сертификатов
   - Создание и провижн инфраструктуры осуществляется через один pipeline

### Hard уровень
1. **Инструменты и автомазизация**
   - Создание инфрастуктуры осуществляется через вынесенные тегированние Terraform модули
   - Используется k8s кластер
   - Доставка приложения осуществляется через Helm
   - Используется Ingress для доступа извне
   - В кластере развернут cert-manager для получения сертификата от Lets encrypt
   - 3 пода с приложением и балансировккой через Service
   - Инфрастуктура в отдельной репозитории
   - CD в отдельном репозитории
   - Переменные для пайплайнов в отдельном репозитории
   
2. **Безопасность**
   - Для хранения токена доступа к Yandex Cloud используется Gitlab CI/CD variables (переменная является masked)

3. **Масштабируемость и отказоустойчивость**
    - Легкая масштабируемость за счет использования  k8s кластера и также поставки приложения в виде Helm чарта

4. **Мониторинг и логирование**
   - Отсутствуют

5. **Согласованность с best practies**
   - Нет разделения на контура
   - Public репозитории
   - Кластер находится в одной зоне (нет отказоустойчивости)
   - CD для доставки helm чартов, ingress и cert-manager через осуществляется через один pipeline

## Screenshots
![Логотип](screenshots/app-2048-first-try.png)
![Логотип](screenshots/kubectl.png)
![Логотип](screenshots/yandex-cloud.png)